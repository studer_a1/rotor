\NeedsTeXFormat{LaTeX2e}
\documentclass[11pt,a4paper]{article}
\usepackage{a4,latexsym}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{verbatim}
\usepackage{hyperref}

\author{A. Studer, Scientific Computing, PSI}
\title{Quantum Mechanical Rotor in a Laser Field}


\begin{document}
\maketitle

\begin{abstract}
The time dependent Schr\"odinger equation for a rigid rotor in a external
alignment and laser field and its mechanical analogon is discussed
\end{abstract}

\section{Definition of Hamiltonian}
The Hamiltonian $H$ of the problem is defined as

\begin{equation}
H = H_0 + H_A + H_D
=
\frac{\hbar^2}{2\Theta} L^2 + a_A \bold d \bold E_A(t) + a_L (\bold d \bold E_L(t))^2
\end{equation}
where $\bold d$ is the electric dipole moment of the rotor, $\bold E_A$ the electric
alignment field and $\bold E_L(t)$ the driving Laser field.

Since we work in polar coordinates, we rewrite the Hamiltonian as\footnote{
No perpendicular component of the polarizability tensor}


\begin{equation}
H = H_0 + H_{ext}    
=
\frac{\hbar^2}{2\Theta} L^2 + h_A(t) cos \theta + h_L(t) cos^2 \theta
\end{equation}

\section{Time Dependent Equation}
For the solution of (2) we make the Ansatz

\begin{equation}
\psi_m(t, \theta, \phi) = \sum_l c_{lm}(t)  Y_l^m (\theta, \phi) e^{-iE_lt/\hbar}    
\end{equation}
where $E_l$ is the energy eigenstate of $H_0$.
Since $[H, SO(2)] = 0$, $m$ remains a good quantum number (i.e is conserved) such that the sum only
runs over $l$ (no transitions in magnetic quantum number, since $\langle m | H_{ext} | m' \rangle = 0$).
\newline
The Schr\"odinger equation in coordinate form reads

\begin{equation}
\dot c_{lm} = \frac{-i}{\hbar} \sum_j \langle lm | H_{ext} | jm \rangle c_{jm}(t) e^{-i(E_j -E_l)t/\hbar}  
\end{equation}

The calculate the Matrix elements, we write $cos \theta = 2 \sqrt \frac{\pi}{3} Y_1^{0}(\theta)$ and
$ cos^2 \theta = \frac{4}{3} \sqrt \frac{\pi}{5} Y_2^0(\theta) + \frac{1}{3} = \frac{4}{3} \sqrt \frac{\pi}{5}Y_2^0(\theta) + \frac{2}{3} \sqrt \pi Y_0^0$

The matrix elements are \footnote{We interchangeably use $l$,$j$, since the rotational angular
momentum equals total angular momentum. (No spin)}
\begin{equation}
\langle lm | H_A| jm \rangle
 =
h_A(t) 2 \sqrt \frac{\pi}{3} \langle lm | Y_1^{0} | jm \rangle
\end{equation}

and

\begin{equation}
\langle lm | H_L | jm \rangle
 =
h_L(t)  \frac{4}{3} \sqrt \frac{\pi}{5} \langle lm | Y_2^{0} | jm \rangle
+
h_L(t)\frac{2}{3} \sqrt \pi \langle lm | Y_0^{0} | jm \rangle
\end{equation}

In the following we assume $m=0$ such that we can drop this index, since
the matrix elements are independent of $m$; they can be expressed with the Wigner 3j symbols 
(the second row of the symbols is also dropped, since it contains only zeros)
\footnote{Note that the relation between expectation values and 3j symbols differ
in a complex conjugation of the last spherical harmonics, so for $| jm\rangle  \ne 0$, the sign
of the last $m$ must be flipped.}
\begin{equation}
\langle l | H_A| j \rangle
 =
h_A(t) 2 \sqrt \frac{\pi}{3} \langle l | Y_1^{0} | j \rangle
= h_A(t) 2 \sqrt \frac{\pi}{3} \sqrt{\frac{3(2l+1)(2j+1)}{4\pi} }(l \, 1 \, j)^2  \quad
\end{equation}

and

\begin{equation}
\langle l | H_L | j \rangle
 =
h_L(t) \frac{4}{3} \sqrt \frac{\pi}{5} \sqrt{\frac{5(2l+1)(2j+1)}{4\pi} } (l \, 2 \, j)^2
+
h_L(t)\frac{2}{3} \sqrt \pi \frac{1}{2} \sqrt \frac{1}{\pi} \delta_{jl}
\end{equation}
The selection rules are in (7): $j = l \pm 1$ and for (8): $|l-2| \le j \le l+2$
additionally, since all $m_i$'s are zero, the sum of the total angular moment quantum numbers
in the 3j symbol must be even, such that for (8): $j=l, j=l \pm 2$.

Hence the diagonal elements are


\begin{equation}
\langle l | H_{ext} | l \rangle = 
h_L(t) \bigg( \frac{2}{3} (2l+1) (l \, 2 \, l)^2 + \frac{1}{3} \bigg)
\end{equation}

The first upper diagonal
\begin{equation}
\langle l | H_{ext} | l+1 \rangle = 
 h_A(t) \sqrt{(2l+1)(2(l+1)+1)}(l \, 1 \, l+1)^2
\end{equation}

The first lower diagonal
\begin{equation}
\langle l | H_{ext} | l-1 \rangle = 
 h_A(t) \sqrt{(2l+1)(2(l-1)+1)}(l \, 1 \, l-1)^2  \quad l \ge 1
\end{equation}

The second upper diagonal
\begin{equation}
\langle l | H_{ext} | l+2  \rangle =
h_L(t) \frac{2}{3}  \sqrt{(2l+1)(2(l+2)+1) } (l \, 2 \, l+2)^2
\end{equation}

The second lower diagonal
\begin{equation}
\langle l | H_{ext} | l-2  \rangle =
h_L(t) \frac{2}{3}  \sqrt{(2l+1)(2(l-2)+1)} (l \, 2 \, l-2)^2  \quad
l \ge 0
\end{equation}

Since $H_{ext}$ is a Hermitian operator, so is its matrix representation; hence either
the lower or upper diagonal calculation is superfluous. In the code however this
knowledge can be used to establish a corresponding unit test.
\newpage

Defining $\omega_0 = \hbar / 2 \Theta$ it follows $E_l/\hbar = \omega_0 l(l+1)$.
Defining $\gamma_L(t) = h_L(t)/\hbar$ and $\gamma_A(t) = h_A(t)/\hbar$ the
$\frac{1}{\hbar}$ in (4) can be absorbed in the Hamiltonian functions such that
the $\gamma$'s have unit Hertz.

\section{Expected Orientation}
To get a classical idea how the molecule rotates, we can calculate the
expectation value $\langle cos \theta \rangle(t)$, indicating where the
tip of the diatomic molecule points to. If the molecule is symmetric,
like e.g $O_2$, the orientation $\theta$ cannot be distinguished from 
$\pi - \theta$; the angle $\varphi$ is undefined (or uniformly distributed)
such that the orientation can be best visualized by a conus.
The expectation value in coordinate form reads
\begin{equation}
2 \sqrt \frac{\pi}{3} \langle \psi_m(t) | Y_1^0 | \psi_m(t) \rangle
=
2 \sqrt \frac{\pi}{3} 
\sum_{lj} \bar c_{l}(t) c_{j}(t) e^{-i(E_j - E_l)t/\hbar}
\langle Y_l^m | Y_1^0 | Y_j^m\rangle 
\end{equation}
which simplifies to ($j = l \pm 1$)

\begin{equation}
\sum_l \bar c_{l}(t) c_{l+1}(t) e^{-i(E_{l+1} - E_l)t/\hbar}
\langle Y_l^m | Y_1^0 | Y_{l+1}^m \rangle
+
\sum_l \bar c_{l}(t) c_{l-1}(t) e^{-i(E_{l-1} - E_l)t/\hbar}
\langle Y_l^m | Y_1^0 | Y_{l-1}^m \rangle
\end{equation}

we increment the counter variable $l$ in the second term, such that
\begin{equation}
\langle cos \theta \rangle(t)
=
4 \sqrt \frac{\pi}{3} Re \bigg(
\sum_l \bar c_{l}(t) c_{l+1}(t) e^{-i(E_{l+1} - E_l)t/\hbar}
\langle Y_l^m | Y_1^0 | Y_{l+1}^m \rangle
\bigg)
\end{equation}

Note that for fixed $l$, we can combine different $m$'s to a new basis
without changing the time evolution of the expansion coefficients.
If e.g we define a new basis with a well defined $\phi \rightarrow -\phi$
parity, $(Y_l^m \pm Y_l^{-m})/\sqrt 2$, then all the $\tilde c_{lm\pm}$ have the same
time evolution as the $c_{lm}$ from eq. (4), where the $m$ label is (also) irrelevant.
Consequently, the expected orientation does not change either. (If the initial
conditions match, i.e $c_{lm}(0) = \tilde c_{lm\pm}(0)$. So starting from e.g
$(Y_1^1 + Y_1^{-1})/\sqrt 2$ leads to the same expected orientation as starting
from $Y_1^1$, which in turn leads to the same results as starting from $Y_1^0$, 
except of course that starting from $Y_1^1$ will only populate states $Y_l^1$).
\newline
Other expectation values, like $\langle L_z \rangle$, $\langle L^2 \rangle$,
$\langle E \rangle$ are easier to calculate: $\langle L_z \rangle = m$,
$\langle \Delta L_z \rangle = 0$;  $\langle L^2 \rangle = \sum_l |c_l|^2 l(l+1)$;
$\langle E \rangle = \hbar \omega \langle L^2 \rangle 
+ h_A \langle cos \theta \rangle + h_L\langle cos^2 \theta \rangle$
(at least if we also calcilated $\langle cos^2 \theta \rangle$)
The relation btween expectation values of spherical harmonics and
Wigner 3j symbol reads
\begin{equation}
\langle Y_{l_1}^{-m_1} | Y_{l_2}^{m_2} | Y_{l_3}^{m_3} \rangle
=
\int_0^{2\pi}\int_0^\pi Y_{l_1}^{m_1}(\theta,\phi)Y_{l_2}^{m_2}(\theta,\phi)Y_{l_3}^{m_3}(\theta,\phi)\sin(\theta)d\theta d\phi
\end{equation}
\begin{equation}
= \,
\sqrt{\frac{(2l_1+1)(2l_2+1)(2l_3+1)}{4\pi}} 
\left( {\begin{array}{ccc} l_1 & l_2 & l_3  \\ 0 & 0 & 0  \\ \end{array} } \right) \left( {\begin{array}{ccc} l_1 & l_2 & l_3  \\ m_1 & m_2 & m_3  \\ \end{array} } \right)
\end{equation}
Example: $t=0, c_0= [\frac{1}{\sqrt 2} , \frac{1}{\sqrt 2},0,0..]$, $\langle cos \theta \rangle(0)=
4 \sqrt \frac{\pi}{3} \frac{1}{2} \frac{3}{2}\frac{1}{\sqrt \pi} (0,1,1)^2 = \frac{1}{\sqrt 3}$



\section{Expected Energy}
The expectation value of the energy of the rotor reads 
\begin{equation}
E = E_{kin} + E_{pot}
=
\langle H_0 \rangle + \langle H_{ext} \rangle
\end{equation}

which evaluates in coordinates as 
\begin{equation}
E = 
\sum_{lj} \bar c_{l}(t) c_{j}(t) e^{-i(E_j - E_l)t/\hbar}
\langle Y_l^m | H_0 | Y_j^m\rangle
+
\sum_{lj} \bar c_{l}(t) c_{j}(t) e^{-i(E_j - E_l)t/\hbar}
\langle Y_l^m | H_{ext} | Y_j^m\rangle
\end{equation}

The first matrix is diagonal, the second matrix equals the time evolution matrix
(up to a factor $i \hbar$)

\begin{equation}
E =
\hbar \omega_0 \sum_l |c_l|^2 l(l+1)
+
i\hbar c^* T c
\end{equation}

where $T$ labels the transition matrix from eq (4). Since this matrix needs to
be defined in the code (to solve the time evolution of the coordinates $c$)
it can be reused for calculating the Energy.


\section{Classical Equation of Motion}
Assuming the rotor in the $xz$ plane and the field in $z$ direction, both
torque and angular momentum only have a $y$ direction. \footnote{Corresponds to $m=0$
in QM model}

For a diatomic molecule

\begin{equation}
L_y = \Theta(t) \omega(t) = \alpha r^2(t) \omega(t)
\end{equation}


\begin{equation}
\bold M = 2 \bold r \times \bold F = 
\bold d \times \bold E =
2eE sin(\omega_L t) r(t) \bold e (\theta(t) ) \times \bold e_z 
\end{equation}

\begin{equation}
M_y = 2eE sin(\omega_L t) r(t) sin(\theta(t)) 
\end{equation}

Leading to the equation of motion
\begin{equation}
M_y = \dot L_y \quad
2eE sin(\omega_L t) r(t) sin(\theta(t)) 
=\alpha \frac{d}{dt} r^2(t) \omega(t)
\end{equation}

If we assume the moment of inertia constant\footnote{
Regarding the moment of inertia, $r$ is constant, whereas
for the dipole, $r$ is time dependent (otherwise there was
no induced dipole moment) hence a contradictory model} as in the QM model
\begin{equation}
\dot \omega(t) = \ddot \theta(t)
= \beta  r(t) sin(\omega_L t)sin(\theta(t)) / \Theta
\end{equation}

with 
\begin{equation}
r(t) = r_0 + \Delta r  sin(\omega_L t) cos(\theta(t))
\end{equation}

being the induced dipole length. (The molecule is stretched/squeezed proportional
to the applied force in the molecule's $z$ direction, $\Delta r \propto \alpha E$) 
For\footnote{negative $r$ meaning dipole moment is inverted} $r_0 \rightarrow 0$,

\begin{equation}
\ddot \theta(t)
\propto  \frac{\alpha}{\Theta}  E_L^2(t) sin(\theta) cos(\theta) 
= \frac{\alpha}{2\Theta}E^2  sin^2(\omega_L t) sin(2\theta)
\end{equation}


since $sin^2(x) = 1/2(1-cos(2x))$
\begin{equation}
\ddot \theta(t)
= \frac{\alpha}{4\Theta}E^2  \big( sin(2\theta) - cos(2\omega_L t)sin(2\theta) \big)
 \big)
\end{equation}
\begin{equation}
\ddot \theta(t)
= \frac{\alpha}{4\Theta}E^2  \bigg
( sin(2\theta) - \frac{1}{2}\big(sin(2\theta - 2\omega_L t) + sin(2\theta + 2\omega_L t) \big)
\bigg)
\end{equation}
Hence $\theta = 0$ is a (unstable) solution, as expected.
Assuming a frequency close to resonance, $\theta(t) = \omega_L t + \Delta \theta(t)$
and expanding to zeroth and first order Taylor
\begin{equation}
\ddot \Delta \theta(t)
= \frac{\alpha}{4\Theta}E^2  \bigg
( sin(2 \omega_L t) - \frac{1}{2}
\big( 2 \Delta \theta(t) + sin(4\omega_L t) \big)
\bigg)
\end{equation}


\begin{equation}
\ddot \Delta \theta(t)
= - \frac{\alpha}{4\Theta}E^2 \Delta \theta(t)
+
\frac{\alpha}{4\Theta}E^2 
\big(
sin(2 \omega_L t)  - \frac{1}{2} sin(4\omega_L t) 
\big)
\end{equation}

So far not included is a damping of the rotation via radiation losses.
The emited power of the dipole is $\propto w^4$, decreasing the kinetic energy
$\propto \omega^2$ of the rotor. Hence the free rotor obeys (due to damping)
$\frac{d}{dt} \omega^2 \propto \omega \dot \omega \propto -\omega^4$ or
$\ddot \theta \propto -\omega^3$. Approximating again $-\omega^3 = -(w_L + \dot \Delta \theta)^3
\simeq  - w_L^3 - 3w_L^2 \dot \Delta \theta \propto \ddot \Delta \theta $, we conclude that
a damping factor $\propto - \dot \Delta \theta$ can be added to (26) (plus a constant), such
that we finally arrive at a forced, damped oscillator describing $\Delta \theta$
(the deviation from the resonance rotation) with stiffness $\frac{\alpha}{4\Theta}E^2$ and unit 'mass'.
The $\Delta \theta$ model is only adequate for $\omega_0 \ll \omega_L$, where $\omega_0$
is defined as the eigenfrequency $\sqrt{\frac{\alpha}{4\Theta}}E$ of the $\Delta \theta$
oscillator\footnote{Contrary to the QMO, the classical free oscillator has no eigenfrequency;
so this $\omega_0$ is not to be confused with the $\omega_0$ defined on page (3)}.
In this case, the driving force is negligable, and the equation is dominated
by the free (damped) oscillation $\propto \omega_0$. Only in this case the Taylor approximations
above are justified for a reasonably large time interval. For the long run, this is
obviously no slution, since the damped (unforced) oscillator amplitude goes to zero, but
$\theta(t) = \omega_L t$ is not a solution of the full equation. (Neither is $\Delta \theta = 0$
a solution to (27))

If there is a alignment field $\propto a(t)$ as well, the equation reads
\begin{equation}
\ddot \theta
=  k_1 I_L(t) sin(2\theta) 
+  k_2 E_A(t) sin(\theta) 
\end{equation}

Numerically solving this equation allows for calculating $cos \, \theta(t)$,
in analogy to $\langle cos \theta \rangle (t)$

\subsection{$L_z$ is conserved}

Analoguous to the quantum mechanical rotor, the classical linear rotor 
angular momentum component $L_z$ is also conserved: 
Since $\bold M = \bold d \times \bold E$, and since
$\bold E = f(t) \bold e_z$, there is $M_z = d_x E_y - d_y E_x = 0$ and
hence $\dot L_z = 0$ such that $L_z$ = const, as in the QM case, where
the eigenvalue $L_z | \psi(t) \rangle = \hbar m | \psi(t) \rangle $ is time independent. 
However, that the (expected) orientation is independent of the initial condition is less
obvious. But starting with a $L_z$ component does not change much, it keeps constant
and the $L_x, L_y$ components solve the same differential equation as for $L_{z,0} =0 $
and the non vanishing $L_z$ component does not affect orientation. (corresponding to
the part of the rotation in the $x,y$ plane where cos $\theta = 0 \, \forall t$).

\end{document}

