import numpy as np
from numpy import linalg as LA
#from py3nj import wigner3j as w3j #wrong module
from sympy.physics.wigner import wigner_3j as w3j
from scipy.integrate import complex_ode
from scipy.special import expit

import matplotlib.pyplot as plt

#Global Constants (related to free Hamiltonian and free states)
l_max = 5 #cutoff angular momentum
w_0 = 1.0 #Basic Frequency of rotor

#Time interval/step. global: only used in 
#Laser and Alignment amplitude (besides main)
t_0 = 0.0; t_f = 20.0; dt = 0.01

#Experiment control
doSwitchOffLaser = False
doModulateLaser = False
doSwitchOffAlignment = False


#Alignment field amplitude [Hz]
def h_A(t):
    if doSwitchOffAlignment:
        return 0.0*t #return array if input is array
    amplitude = 1.0
    rampupTime = 0.5
    return amplitude*expit((t-rampupTime)/(rampupTime/np.exp(1)))

#Laser field intensity [Hz]
def h_L(t):
    if doSwitchOffLaser:
        return 0.0*t
    #laser frequency; resonance: delta E_rotor = l_f(l_f+1) - l_i(l_i+1) = delta E_phot
    #For l_i=0: delta E_rot=l_f(l_f+1). delta E_phot = 2 hw_l since 2 photons are absorbed in transition.
    #(H_L ~ I_L ~ E_L^2 ~ a^2 + a*^2 + aa* + a*a = 'absorb(=drive) + emit(=damp) + scattering')
    #w_L = w_0 #pseudo-resonant for l_i=0 -> l_f=1 transition, but rotor part of Laser-Matrix element=0.
               #To check in simulation that this is not resonant, switch off alignment and run..
    w_L = 3*w_0 #resonant: <0,l_f=2| a_L^2 cos^2 theta | 2 hw_L,l_i=0>
                # -> delta E_L = 2*hw_L = 2*3 hw_0 = 2(2+1)hw_0 = delta E_rotor
                #We can also start in state l_i=2 to see 'stimulated emision' (ground state populated);
                #setting l_i = 2 and w_L = 7*w_0, we see the next resonant transition (2 -> 4)
    #w_L = 11*w_0  #off resonant
    amplitude = 10   #constant amplitude
    if doModulateLaser:
        amplitude = 0.1*np.exp(-(t-0.5*(t_f-t_0))**2/(t_f-t_0)) #modulated amplitude
    return amplitude*np.sin(w_L*t)**2

#Static part of transition Matrix
def setUpTransitionMatrix():
    V = np.zeros((l_max, l_max)).astype('complex128')
    #diagonal
    for l in range(0,l_max):
        V[l,l] = 2/3.0*2*(l+1) * w3j(l,2,l,0,0,0)**2 + 1/3.0
    #first upper diagonal
    for l in range(0,l_max-1):
        V[l,l+1] = np.sqrt((2*l+1)*(2*(l+1)+1)) * w3j(l,1,l+1,0,0,0)**2
    #first lower diagonal
    for l in range(1,l_max):
        V[l,l-1] = np.sqrt((2*l+1)*(2*(l-1)+1)) * w3j(l,1,l-1,0,0,0)**2
    #second upper diagonal
    for l in range(0,l_max-2): 
        V[l,l+2] = 2/3.0*np.sqrt((2*l+1)*(2*(l+2)+1)) * w3j(l,2,l+2,0,0,0)**2
    #second lower diagonal
    for l in range(2,l_max): 
        V[l,l-2] = 2/3.0*np.sqrt((2*l+1)*(2*(l-2)+1)) * w3j(l,2,l-2,0,0,0)**2
        
    return V
    
#Time dependent part of transition Matrix
def setUpDynamicTransitionMatrix(t, V):
    V_t = np.copy(V)
    #diagonal
    for l in range(0,l_max):
        V_t[l,l] *= h_L(t)
    #first upper diagonal
    for l in range(0,l_max-1):
        V_t[l,l+1] *= h_A(t) * np.exp(-1j*w_0*t*((l+1)*(l+1+1) - l*(l+1)))
    #first lower diagonal
    for l in range(1,l_max):
        V_t[l,l-1] *= h_A(t) * np.exp(-1j*w_0*t*((l-1)*(l-1+1) - l*(l+1)))
    #second upper diagonal
    for l in range(0,l_max-2):
        V_t[l,l+2] *= h_L(t) * np.exp(-1j*w_0*t*((l+2)*(l+2+1) - l*(l+1)))
    #second lower diagonal
    for l in range(2,l_max):
        V_t[l,l-2] *= h_L(t) * np.exp(-1j*w_0*t*((l-2)*(l-2+1) - l*(l+1)))
        
    V_t *= -1j
    return V_t
    
def calculateFreeHamiltonian():
    #The same as for interaction picture
    H_0 = np.zeros((l_max, l_max)).astype('complex128')
    for l in range(0,l_max):
        H_0[l,l] += w_0*l*(l+1)
    return H_0

def calculateExternalHamiltonian(t, V):
    H_ext = np.copy(V)
    #diagonal
    for l in range(0,l_max):
        H_ext[l,l] *= h_L(t)
    #first upper diagonal
    for l in range(0,l_max-1):
        H_ext[l,l+1] *= h_A(t)
    #first lower diagonal
    for l in range(1,l_max):
        H_ext[l,l-1] *= h_A(t)
    #second upper diagonal
    for l in range(0,l_max-2):
        H_ext[l,l+2] *= h_L(t)
    #second lower diagonal
    for l in range(2,l_max):
        H_ext[l,l-2] *= h_L(t)
        
    return H_ext
    
def calculateHamiltonian(t, V):
    return calculateFreeHamiltonian() + calculateExternalHamiltonian(t, V)
    
def calculateHamiltonianInInteractionPicture(t, V):
    H_t = setUpDynamicTransitionMatrix(t, V)
    H_ext = 1j*H_t
    H_0 = calculateFreeHamiltonian()#same form in both schrödinger and interaction
    return H_0 + H_ext   
    
def calculateIntegrationStep(t, c): #this function does not need the 'copy' above
    H = setUpTransitionMatrix() #Not necessary to recalculate
    H_t = setUpDynamicTransitionMatrix(t, H)
    return np.dot(H_t, c)

def calculateIntegrationStep_arg(t, c, H): #can be used with the scipy bugfix
    H_t = setUpDynamicTransitionMatrix(t, H)
    return np.dot(H_t, c)

def calculateExpectedOrientation(c, t):
    l_m = c.shape[0]
    C = np.zeros((l_m, l_m)).astype('complex128')
    #first upper diagonal
    for l in range(0,l_max-1):
        C[l,l+1] = np.sqrt( (2*l+1)*3*(2*(l+1)+1)/(4*np.pi) ) * w3j(l,1,l+1,0,0,0)**2
        C[l,l+1] *= np.exp( -1j*w_0*t*( (l+1)*(l+1+1) - l*(l+1) ) )
    
    return 4*np.sqrt(np.pi/3.0) * ( np.dot(c.conj(), np.dot(C, c)) ).real
    
def calculateExpectedEnergy(c, t, V):
    H = calculateHamiltonianInInteractionPicture(t, V)
    E = np.dot(c.conj(), np.dot(H, c))
    np.testing.assert_allclose(E.imag, 0.0, atol=1e-10)
    return E.real
    
def calculateAdiabaticGroundStateEnergy(t, V):
    H = calculateHamiltonian(t, V)
    w, v = LA.eig(H)
    E = w[w.argmin()] #Ground state energy (assume that we start in ground state)
    np.testing.assert_allclose(E.imag, 0.0, atol=1e-10)
    #As a byproduct, calculate also expected orientation
    C = np.zeros_like(V)
    #first upper diagonal
    for l in range(0,l_max-1):
        C[l,l+1] = np.sqrt( (2*l+1)*3*(2*(l+1)+1)/(4*np.pi) ) * w3j(l,1,l+1,0,0,0)**2
    c = v[:, w.argmin()]
    eo = 4*np.sqrt(np.pi/3.0) * ( np.dot(c.conj(), np.dot(C, c)) )
    np.testing.assert_allclose(eo.imag, 0.0, atol=1e-10)
    p = np.abs(c)**2
    np.testing.assert_allclose(p.sum(), 1.0, rtol=1e-03)
    return E.real, eo.real, p

def unitTestHermitian():
    V = setUpTransitionMatrix()
    #print (np.array2string(V, precision=2) )
    #V is hermitian
    np.testing.assert_allclose(V, V.conj().T)
    #V is real
    np.testing.assert_allclose(V, V.conj())
    
def unitTestUnitary(p):
    #Verify that time evolution is unitary. Sum_n |cn|^2 = 1 for all t
    #though this is not perfectly true, since we truncate at l_max
    np.testing.assert_allclose(p.sum(axis=1), np.ones(p.shape[0]), rtol=1e-03)


#f-Wrapper to circumvent Scipy's complex_ode bug
class funcWithArg(object):
    def __init__(self, f, fargs=[]):
        self._f = f
        self.fargs = fargs 
        
    def f(self, t, y):
        return self._f(t, y, *self.fargs)
        

def runSimulation(c_0):
    #solver
    #r = complex_ode(calculateIntegrationStep)
    #r.set_f_params(H) #a bug in scipy?
    H_T = setUpTransitionMatrix()
    #Workaround for the above line
    calculateIntegrationStep_class = funcWithArg( calculateIntegrationStep_arg, fargs = [H_T] )
    r = complex_ode(calculateIntegrationStep_class.f)
    #r = complex_ode(calculateIntegrationStep)
    r.set_initial_value(c_0, t_0)
    #Save calculations (to plot)
    c_T = c_0  #expansion coefficients
    T = t_0     #Time stamps
    while r.successful() and r.t < t_f:
        c = r.integrate(r.t+dt)
        c_T = np.vstack( (c_T, c) )
        T = np.vstack(( T, r.t) )        
    
    return T, c_T


def calculateSimulationDerivedResults(T, c_T):
    #calculate probability |c_l|^2
    l_probabilites = (np.abs(c_T))**2
    unitTestUnitary(l_probabilites)
    H_T = setUpTransitionMatrix()
    #calculate <cos theta>
    eo = np.zeros(c_T.shape[0])
    E = np.zeros(c_T.shape[0])
    E_a = np.zeros(c_T.shape[0])
    eo_a = np.zeros(c_T.shape[0])
    p_a = np.zeros_like(l_probabilites)
    for t_i in range(T.shape[0]):
        eo[t_i] = calculateExpectedOrientation(c_T[t_i,:], T[t_i])
        E[t_i] = calculateExpectedEnergy(c_T[t_i,:], T[t_i], H_T)
        E_a[t_i], eo_a[t_i], p_a[t_i] = calculateAdiabaticGroundStateEnergy(T[t_i], H_T)

    return eo, l_probabilites, E, E_a, eo_a, p_a

def plotSimulationResults(T, E_A, I_L, eo, p, E, E_a, eo_a, p_a):
    #Plot    
    plt.subplot(8, 1, 1)
    plt.ylabel('Alignment')
    #plt.xticks([])  
    plt.plot(T, E_A, color='orange')
    plt.subplot(8, 1, 2)
    plt.ylabel('Laser')
    plt.plot(T, I_L, color='r')
    plt.subplot(8, 1, 3)
    plt.ylabel('Orient.')
    plt.plot(T, eo)
    plt.subplot(8, 1, 4)
    plt.ylabel('Adiabatic\n Orient.')
    plt.plot(T, eo_a, color='b')
    plt.subplot(8, 1, 5)
    plt.plot(T, p)
    plt.ylabel('Prob.')
    #plt.legend(labels = ["l = " + str(l) for l in range(l_max)])
    plt.subplot(8, 1, 6)
    plt.plot(T, p_a)
    plt.ylabel('Adiabatic\n Prob.')
    plt.legend(labels = ["l = " + str(l) for l in range(l_max)])
    plt.subplot(8, 1, 7)
    plt.ylabel('Energy')
    plt.plot(T, E, color='m')
    plt.subplot(8, 1, 8)
    plt.ylabel('Adiabatic\n Energy')
    plt.plot(T, E_a, color='c')

    plt.xlabel('time')
    plt.show()
    

if __name__ == "__main__":
    unitTestHermitian()
    #initial conitions
    c_0 = np.zeros(l_max)
    groundState = np.zeros(l_max)
    groundState[0] = 1.0 #Ground state only
    c_0 = groundState
    #c_0 = np.ones(l_max);  #uniformly populated, erroneous results, since l_max unjustified
    #c_0 = np.zeros(l_max)
    #c_0[:-2] = 1;  #uniformly populated,  l_max (better) justified
    #c_0[0::2] = 1  #even l's only => <cos theta> = 0 if no Alignment
    #c_0[1::2] = 1  #odd l's only  =>                """             (?)
    #Normalize c
    c_0 /= np.sqrt( (np.abs(c_0)**2).sum() )
    #print ( c_0, "  ", calculateExpectedOrientation(c_0, 0.0) ) #Initial orientation
    T, c_T = runSimulation(c_0)
    eo, p, E, E_a, eo_a, p_a = calculateSimulationDerivedResults(T, c_T)   
    E_A = h_A(T)
    I_L = h_L(T)
    plotSimulationResults(T, E_A, I_L, eo, p, E, E_a, eo_a, p_a)
    
